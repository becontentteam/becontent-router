<?php

namespace becontent\router;

use becontent\core\control\Settings as Settings;

class Router {
	private $routingTable;
	private $controllersTable;
	
	private static $instance;
	
	
	public static function getInstance() {
		if (! isset ( self::$instance ))
			self::$instance = new Router ();
		return self::$instance;
	}
	
	
	public function __construct() {
		$this->routingTable = array ();
		$this->controllersTable=array();
	}
	
	
	public function dispatch($route, $parameters) {
		
		if (file_exists ( $this->routingTable [$route] )) {
			
			require_once $this->routingTable [$route];
		} else {
			
			if(isset($this->controllersTable [$route]))
			{
				
				$requestedController=$this->controllersTable [$route];		
				$requestedController->setIncomingData($parameters);
				$requestedController->doTheMagic();
			}
			else{
				Settings::setActualAppRoot ( dirname ( $this->routingTable [Settings::getDefaultErrorRoute ()] ) . "/.." );
				Settings::setSkinsPath ( dirname ( $this->routingTable [Settings::getDefaultErrorRoute ()] ) . "/../skins" );
				require_once $this->routingTable [Settings::getDefaultErrorRoute ()];
			}
		}
	}
	
	
	public function loadRoutingTables() {
		
	}
	
	public function injectControllersTable($controllersTable) {
		$this->controllersTable = array_merge ( $this->controllersTable, $controllersTable );
	}
	
	
	public function injectRoutingTable($routingTable) {
		$this->routingTable = array_merge ( $this->routingTable, $routingTable );
	}
}
?>